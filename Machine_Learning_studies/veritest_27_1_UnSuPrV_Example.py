#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 29 16:39:36 2023

@author: cfxm
"""
import requests as requ
import pandas as pd
from bs4 import BeautifulSoup as bs
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from yellowbrick.cluster import KElbowVisualizer
from sklearn.cluster import KMeans, AgglomerativeClustering
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.cluster.hierarchy import linkage, dendrogram

# url = "https://www.isyatirim.com.tr/tr-tr/analiz/hisse/Sayfalar/Temel-Degerler-Ve-Oranlar.aspx?endeks=03#page-1"

# req = requ.get(url)
# # print(req)  #<Response [200]>

# s = bs(req.text, "html.parser")
# table = s.find("table", {"id": "summaryBasicData"})
# table2 = pd.read_html(str(table), flavor="bs4")[0]

# # print(table2)
# sharenames = []
# for i in table2["Kod"]:
#     sharenames.append(i)


# parameters = (
#     ("hisse", sharenames[0]),
#     ("startdate", "23-01-2023"),
#     ("enddate", "28-09-2023")
# )
# url2 = "https://www.isyatirim.com.tr/_layouts/15/Isyatirim.Website/Common/Data.aspx/HisseTekil?"
# req2 = requ.get(url2, params=parameters).json()["value"]
# s_data = pd.DataFrame.from_dict(req2)
# # print(s_data)

# s_data = s_data.iloc[:, 0:3]
# s_data = s_data.rename({"HGDG_HS_KODU": "ShareName",
#                        "HGDG_TARIH": "Date_", "HGDG_KAPANIS": "Price"}, axis=1)
# # print(s_data)
# sd_data = {"Date_": s_data["Date_"], s_data["ShareName"][0]: s_data["Price"]}
# snd_data = pd.DataFrame(sd_data)
# # print(snd_data)

# del sharenames[0]

# all_data2 = snd_data.copy()
# for j in sharenames:
#     parameters2 = (
#         ("hisse", j),
#         ("startdate", "23-01-2023"),
#         ("enddate", "28-09-2023")
#     )
#     req3 = requ.get(url2, params=parameters2).json()["value"]
#     s_data2 = pd.DataFrame.from_dict(req3)
#     # print(s_data2)

#     s_data2 = s_data2.iloc[:, 0:3]
#     s_data2 = s_data2.rename({"HGDG_HS_KODU": "ShareName",
#                               "HGDG_TARIH": "Date_", "HGDG_KAPANIS": "Price"}, axis=1)

#     sd_data2 = {"Date_": s_data2["Date_"],
#                 s_data2["ShareName"][0]: s_data2["Price"]}
#     # print(sd_data2)
#     all_data2 = pd.merge(all_data2, pd.DataFrame(sd_data2), on="Date_")


# print(all_data2)
# all_data2.to_excel("shares_bist30.xlsx")

data_2 = pd.read_excel("shares_bist30.xlsx")
shares_data = data_2.copy()
# print(shares_data)
shares_data = shares_data.drop(columns=["Date_", "Unnamed: 0"], axis=1)
# print(shares_data)
income = shares_data.pct_change().mean()*252
result = pd.DataFrame(income)
result.columns = ["income"]
result["Volatility"] = shares_data.pct_change().std()*np.sqrt(252)
result = result.reset_index()
result = result.rename({"index": "Sharename"}, axis=1)
# print(result)

mmsc = MinMaxScaler()
X = mmsc.fit_transform(result.iloc[:, [1, 2]])
# print(X)
X = pd.DataFrame(X, columns=["income", "Volatility"])
# print(X.describe())

# kmodel = KMeans(random_state=0)
# # graph = KElbowVisualizer(kmodel, k=(2, 20))
# # graph.fit(X)
# # graph.poof() # elbow k=6
# kmodel2 = KMeans(n_clusters=5, random_state=0)
# kfit = kmodel2.fit(X)
# labels = kfit.labels_

# result["labels"] = labels
# # sns.scatterplot(x="income", y="Volatility", data=X, hue=labels, palette="deep")
# sns.scatterplot(x="labels", y="Sharename", data=result,
#                 hue=labels, palette="deep")
# plt.show()
# hc = linkage(X, method="single")
# dendrogram(hc)  # clus  4
# plt.show()

modelAgg = AgglomerativeClustering(n_clusters=4, linkage="single")
pred = modelAgg.fit_predict(X)
labels2 = modelAgg.labels_
result["Labels"] = labels2

sns.scatterplot(x="Labels", y="Sharename", data=result,
                hue="Labels", palette="deep")
plt.show()

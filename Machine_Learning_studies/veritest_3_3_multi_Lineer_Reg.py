#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 22:31:42 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts
from sklearn.linear_model import LinearRegression as linreg
import sklearn.metrics as mtr
data = sns.load_dataset("tips")
tip_data = data.copy()
# print(tip_data)
# print(tip_data.isnull().sum())
# print(tip_data.dtypes)

categories = []
categorical = tip_data.select_dtypes(include=["category"])
for i in categorical.columns:
    categories.append(i)

# print(categories)
tip_data = pd.get_dummies(tip_data, columns=categories, drop_first=True)
# print(tip_data)
y = tip_data.tip
X = tip_data.drop(columns="tip", axis=1)

X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)
lr = linreg()
lr.fit(X_train, y_train)
predict = lr.predict(X_test)
y_test = y_test.sort_index()

df = pd.DataFrame({"Real": y_test, "Predict": predict})
df.plot(kind="line")
print(mtr.r2_score(y_test, predict))

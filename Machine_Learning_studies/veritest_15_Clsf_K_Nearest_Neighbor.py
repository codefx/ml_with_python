#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 22:48:55 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score


data = pd.read_csv("cancerdata.csv")  # same as logreg_data
cancer_data = data.copy()
# print(cancer_data.isnull().sum())
# print(cancer_data.head())
# print(cancer_data.info())
cancer_data = cancer_data.drop(columns=["id", "Unnamed: 32"], axis=1)
# print(cancer_data.info())
cancer_data.diagnosis = [1 if value ==
                         "M" else 0 for value in cancer_data.diagnosis]
# print(cancer_data)
# M = cancer_data[cancer_data["diagnosis"] == "M"]
# B = cancer_data[cancer_data["diagnosis"] == "B"]
# plt.scatter(M.radius_mean, M.texture_mean, color="red", label="Kötü huylu")
# plt.scatter(B.radius_mean, B.texture_mean, color="green", label="İyi huylu")
# plt.legend()
# plt.show()


y = cancer_data.diagnosis
X = cancer_data.drop(columns="diagnosis", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stsc = StandardScaler()
X_train = stsc.fit_transform(X_train)
X_test = stsc.transform(X_test)


model_knn = KNeighborsClassifier()
model_knn.fit(X_train, y_train)
pred = model_knn.predict(X_test)

accu_score = accuracy_score(y_test, pred)
print(accu_score*100)  # 94.73684210526315

success = []
for k in range(1, 20):
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train, y_train)
    pred2 = knn.predict(X_test)
    success.append(accuracy_score(y_test, pred2))

# print(success)
# print(max(success))
top = success.index(max(success))+1
print(top)
# plt.plot(range(1, 20), success)
# plt.xlabel("K")
# plt.ylabel("Success")
# plt.show()

model_knn3 = KNeighborsClassifier(n_neighbors=top)
model_knn3.fit(X_train, y_train)
pred3 = model_knn3.predict(X_test)

accu_score = accuracy_score(y_test, pred3)
print(accu_score*100)  # 96.49122807017544

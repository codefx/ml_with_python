#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 16:50:14 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.tree import DecisionTreeRegressor as dectreg, plot_tree


data = pd.read_csv("Position_Salaries.xls")
sp_data = data.copy()

y = sp_data.Salary
X = sp_data.Level


y = np.array(y).reshape(-1, 1)
X = np.array(X).reshape(-1, 1)

# dtr = dectreg(random_state=0)
# dtr = dectreg(random_state=0, max_leaf_nodes=3)
dtr = dectreg(random_state=0, max_leaf_nodes=5)
dtr.fit(X, y)

pred = dtr.predict(X)

# plt.scatter(X, y, color="Blue")
# plt.plot(X, pred)
# plt.show()
plt.figure(figsize=(200, 100), dpi=100)
plot_tree(dtr, feature_names=["Level"],
          class_names=["Salary"], rounded=True, filled=True)  # names must be in list->[]
plt.show()

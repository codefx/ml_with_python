#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  2 13:25:25 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import re
from textblob import TextBlob
from googletrans import Translator  # ,LANGUAGES
from time import sleep

data = pd.read_csv("twitter_corrected_dataset.csv")
twit_data = data.copy()
# print(twit_data.head(10))

# sents = []
# for i in range(3, 7):
#     sent = twit_data["Text"][i]
#     sents.append(sent)

# for i in sents:
#     blob = TextBlob(i)
#     print(i)
#     print(blob.sentiment)
#     print("---------------")


def cleanTweet(twit):
    # tweet = re.sub('#[a-zA-ZÇŞĞÖÜİöüğşç0-9]+', " ", twit) for tr lang
    tweet = re.sub('#[a-zA-Z0-9]+', " ", twit)  # for hashtag
    tweet = re.sub('\\n', " ", tweet)  # for \\n
    tweet = re.sub('@[\S]*', " ", tweet)  # for mention @xyz
    tweet = re.sub('https?:\/\/\S+', " ", tweet)  # for links
    tweet = tweet.lower()  # for lowering
    tweet = re.sub('[^a-zA-Z0-9]', " ", tweet)  # for punctuation
    tweet = re.sub('^[\s]+|[\s]+$', " ", tweet)  # for whitespace
    return tweet


twit_data["Cleaned_Text"] = twit_data["Text"].apply(cleanTweet)
# print(twit_data.head(20))
# print(LANGUAGES)
# translate = Translator()
# word = "Hello, I'm an engineer,in downtown" #Merhaba, şehir merkezinde bir mühendisim
# res = translate.translate(word, src="en", dest="tr")
# print(res.text)

# twit_data2 = twit_data.iloc[0:500:, :]
twit_data2 = twit_data.copy()


def translating(tweet):

    try:
        t = Translator()
        result = t.translate(tweet, src="en", dest="tr")
        sleep(0.5)
        return result.text
    except TypeError:
        return "An error occured."


# twit_data2["To_tr_translated"] = twit_data2["Cleaned_Text"].apply(translating)
# print(twit_data2["To_tr_translated"].head(5))
def sentimentScore(twit):
    blob = TextBlob(twit)
    return blob.sentiment.polarity


twit_data2["Sentiment_Score"] = twit_data2["Cleaned_Text"].apply(
    sentimentScore)


def sent_polar(polarity):
    if polarity < 0:
        return "Negative"
    elif polarity > 0:
        return "Positive"
    else:
        return "Neutral"


twit_data2["Sentiment_Status"] = twit_data2["Sentiment_Score"].apply(
    sent_polar)
print(twit_data2)
twit_data2["Sentiment_Status"].value_counts().plot(kind="bar")
plt.show()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 16:50:14 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.tree import DecisionTreeRegressor as dectreg, plot_tree
from sklearn.model_selection import train_test_split as tts, GridSearchCV
import sklearn.metrics as mtr


data = pd.read_csv("IceCreamData.csv")
sp_data = data.copy()

X = sp_data.Temperature
y = sp_data.Revenue


X_train, X_test, y_train, y_test = tts(X, y, test_size=0.1, random_state=42)


model_dtr = dectreg(random_state=0, max_leaf_nodes=21, min_samples_split=17)
model_dtr.fit(X_train.values.reshape(-1, 1), y_train.values.reshape(-1, 1))

pred = model_dtr.predict(X_test.values.reshape(-1, 1))
# print(pred) for reshape is needed?

r2 = mtr.r2_score(y_test, pred)
mse = mtr.mean_squared_error(y_test, pred)
# R2 : 0.96447587862359  MSE : 1333.2485215987253
print(f" R2 : {r2}  MSE : {mse}")
# R2 : 0.9789043773351418  MSE : 791.7354924027405 after gridcv
parameters = {"min_samples_split": range(
    2, 50), "max_leaf_nodes": range(2, 50)}

grid = GridSearchCV(estimator=model_dtr, param_grid=parameters, cv=10)
grid.fit(X_train.values.reshape(-1, 1), y_train.values.reshape(-1, 1))
# print(grid.best_params_)  # {'max_leaf_nodes': 21, 'min_samples_split': 17}
# plt.scatter(X, y, color="Blue")
# plt.plot(X, pred)
# plt.show()
# plt.figure(figsize=(200, 100), dpi=100)
# plot_tree(dtr, feature_names=["Level"],
#           class_names=["Salary"], rounded=True, filled=True)  # names must be in list->[]
# plt.show()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 16:05:44 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, KFold, cross_val_score as cvs
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression
import sklearn.metrics as mtr


data = pd.read_csv("babies_food_Ingredients.csv")
i_data = data.copy()
# print(i_data)
i_data["size"].fillna(1, inplace=True)
bfi_data = i_data.drop(columns="Unnamed: 0", axis=1)
# print(bfi_data)


# print(bfi_data.isnull().sum())
# print(bfi_data.head())

y = bfi_data.quality
X = bfi_data.drop(columns="quality", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)


sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

pca = PCA(n_components=16)
# pca = PCA()
X_train2 = pca.fit_transform(X_train)
X_test2 = pca.transform(X_test)

print(np.cumsum(pca.explained_variance_ratio_)*100)

lr = LinearRegression()
lr.fit(X_train2, y_train)

pred = lr.predict(X_test2)

r2 = mtr.r2_score(y_test, pred)
rmse = mtr.mean_squared_error(y_test, pred, squared=True)

print(f"r2 : {r2}  rmse : {rmse}")

cv = KFold(n_splits=10, shuffle=True, random_state=1)
lr2 = LinearRegression()
RMSE = []
for i in range(1, X_train2.shape[1]+1):
    err = np.sqrt(-1*cvs(lr2, X_train2[:, :i], y_train.ravel(),
                         cv=cv, scoring="neg_mean_squared_error").mean())
    RMSE.append(err)


plt.plot(RMSE, "-x")
plt.xlabel("Bileşen sayısı")
plt.ylabel("RMSE")
plt.show()

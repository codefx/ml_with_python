#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 14:17:48 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, cross_val_score as cvs
from sklearn.linear_model import LinearRegression, Lasso, Ridge, ElasticNet

import sklearn.metrics as mtr
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import variance_inflation_factor as varinf

data = pd.read_csv("USA_Housing.csv")
h_data = data.copy()
# print(h_data.info())
h_data = h_data.drop(columns="Address", axis=1)
# print(h_data.head())
# print(h_data.isnull().sum())
# sns.pairplot(h_data)
# plt.show()
# corel = h_data.corr()
# sns.heatmap(corel, annot=True)
# plt.show()

y = h_data.Price
X = h_data.drop(columns="Price", axis=1)
# constant = sm.add_constant(X)

# vif = pd.DataFrame()
# vif["Variables"] = X.columns
# vif["VIF"] = [varinf(constant, i+1) for i in range(X.shape[1])]
# print(vif)

X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)


def cross_val(model, cv=10):
    accuracy = cvs(model, X, y, cv=cv)
    return accuracy.mean()


def closeness(trueness, prediction):
    rmse = mtr.mean_squared_error(trueness, prediction, squared=True)
    r2 = mtr.r2_score(trueness, prediction)
    return [rmse, r2]


lin_model = LinearRegression()
lin_model.fit(X_train, y_train)
lin_pred = lin_model.predict(X_test)

ridge_model = Ridge(alpha=0.1)
ridge_model.fit(X_train, y_train)
ridge_pred = ridge_model.predict(X_test)

lasso_model = Lasso(alpha=0.1)
lasso_model.fit(X_train, y_train)
lasso_pred = lasso_model.predict(X_test)

elas_model = ElasticNet(alpha=0.1)
elas_model.fit(X_train, y_train)
elas_pred = elas_model.predict(X_test)

results = [["Linear Model", closeness(y_test, lin_pred)[0], closeness(y_test, lin_pred)[1], cross_val(lin_model)],
           ["Ridge Model", closeness(y_test, ridge_pred)[0], closeness(
               y_test, ridge_pred)[1], cross_val(ridge_model)],
           ["Lasso Model", closeness(y_test, lasso_pred)[0], closeness(
               y_test, lasso_pred)[1], cross_val(lasso_model)],
           ["ElasticNet Model", closeness(y_test, elas_pred)[0], closeness(y_test, elas_pred)[1], cross_val(elas_model)],]

pd.options.display.float_format = '{:.4f}'.format
results = pd.DataFrame(results, columns=["Models", "RMSE", "R2", "Validation"])
print(results)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 17:50:59 2023

@author: cfxm
"""
import pandas as pd
from sklearn.impute import SimpleImputer
import numpy as np
from sklearn.model_selection import train_test_split as tts
from sklearn.linear_model import LinearRegression as linreg
import sklearn.metrics as mtr


data = pd.read_csv("advertising_2.xls")
adv_data2 = data.copy()
# print(adv_data2.isnull().sum())  #boş değer kontrolu

imputer = SimpleImputer(missing_values=np.nan, strategy="mean")
imputer = imputer.fit(adv_data2)
# loc etiket ile ulaşmak için, iloc satır-sutun index no ile ulaşmak için
adv_data2.iloc[:, :] = imputer.transform(adv_data2)
# print(adv_data2.isnull().sum())
# print(adv_data2.head(12))
y = adv_data2.Sales
X = adv_data2[["TV", "Radio"]]
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)
lr = linreg()
lr.fit(X_train, y_train)
predict = lr.predict(X_test)

r2 = mtr.r2_score(y_test, predict)
mse = mtr.mean_squared_error(y_test, predict)
rmse = mtr.mean_squared_error(y_test, predict, squared=False)
mae = mtr.mean_absolute_error(y_test, predict)

print(f"r2 : {r2}  mse : {mse}\nrmse : {rmse} mae : {mae}")

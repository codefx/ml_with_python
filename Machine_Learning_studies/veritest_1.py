#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 18 22:12:09 2023

@author: cfxm
"""

import pandas as pd
from sklearn.model_selection import train_test_split

data = pd.read_excel("./veri_test_1.xlsx")
# print(data)
y = data["Y"]
X = data[["X1", "X2"]]

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42)
# print(X_train)
# print(X_test)

# random_state=42 is important,else every print different result.
print(X_train.sum())

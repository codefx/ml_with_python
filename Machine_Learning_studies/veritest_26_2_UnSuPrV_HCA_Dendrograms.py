#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 29 15:56:42 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from scipy.cluster.hierarchy import linkage, dendrogram
from sklearn.cluster import AgglomerativeClustering

data = pd.read_csv("Iris.csv")
iris_data = data.copy()
X = iris_data.drop(columns=["Id", "Species"], axis=1)
# print(X)

hc_single = linkage(X, method="single")
hc_complete = linkage(X, method="complete")
hc_average = linkage(X, method="average")
hc_centroid = linkage(X, method="centroid")
hc_median = linkage(X, method="median")
hc_ward = linkage(X, method="ward")

# fig, axes = plt.subplots(2, 3)  # 0-0, 0-1,0-2,/ 1-0,1-1,1-2
# dendrogram(hc_single, ax=axes[0, 0])
# axes[0, 0].set_title("Single")
# dendrogram(hc_complete, ax=axes[0, 1])
# axes[0, 1].set_title("complete")
# dendrogram(hc_average, ax=axes[0, 2])
# axes[0, 2].set_title("average")
# dendrogram(hc_centroid, ax=axes[1, 0])
# axes[1, 0].set_title("centroid")
# dendrogram(hc_median, ax=axes[1, 1])
# axes[1, 1].set_title("median")
# dendrogram(hc_ward, ax=axes[1, 2])
# axes[1, 2].set_title("ward")
# plt.show()
model = AgglomerativeClustering(n_clusters=2, linkage="average")
pred = model.fit_predict(X)
# print(pred)

labels = model.labels_
sns.scatterplot(x="SepalLengthCm", y="SepalWidthCm",
                data=X, hue=labels, palette="deep")
plt.show()

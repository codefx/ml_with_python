#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 19:14:41 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt

import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, GridSearchCV
import sklearn.metrics as mtr
from sklearn.tree import DecisionTreeRegressor as decitreg
from sklearn.ensemble import BaggingRegressor

data = pd.read_csv("advertising.xls")
adv_data = data.copy()


y = adv_data.Sales
X = adv_data.drop(columns="Sales", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)
# =============================================================================
# decision tree regression
# =============================================================================
dtr_model = decitreg(random_state=0, max_leaf_nodes=18, min_samples_split=4)
dtr_model.fit(X_train, y_train)
dtr_pred = dtr_model.predict(X_test)

r2 = mtr.r2_score(y_test, dtr_pred)
rmse = mtr.mean_squared_error(y_test, dtr_pred, squared=False)

print(f"       DTR -> R2 : {r2}  RMSE : {rmse}")

# =============================================================================
# bagging decision tree regression
# =============================================================================
bg_model = BaggingRegressor(random_state=0, n_estimators=23)
bg_model.fit(X_train, y_train)
bg_pred = bg_model.predict(X_test)

r2_bg = mtr.r2_score(y_test, bg_pred)
rmse_bg = mtr.mean_squared_error(y_test, bg_pred, squared=False)

print(f"Bagging T Reg-> R2 : {r2_bg}  RMSE : {rmse_bg}")

# =============================================================================
# grid section
# =============================================================================
# parameters1 = {"min_samples_split": range(
#     2, 25), "max_leaf_nodes": range(2, 25)}
# grid1 = GridSearchCV(estimator=dtr_model, param_grid=parameters1, cv=10)
# grid1.fit(X_train, y_train)
# print(grid1.best_params_)  # {'max_leaf_nodes': 18, 'min_samples_split': 4}

# parameters2 = {"n_estimators": range(2, 25)}
# grid2 = GridSearchCV(estimator=bg_model, param_grid=parameters2, cv=10)
# grid2.fit(X_train, y_train)
# print(grid2.best_params_)  # {'n_estimators': 23}

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 14:47:00 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR


data = pd.read_csv("Position_Salaries.xls")
sp_data = data.copy()

y = sp_data["Salary"]
X = sp_data["Level"]

y = np.array(y).reshape(-1, 1)
X = np.array(X).reshape(-1, 1)


ssx = StandardScaler()
ssy = StandardScaler()

y = ssy.fit_transform(y)
X = ssx.fit_transform(X)

# svr_model = SVR()
# svr_model = SVR(kernel="linear")
# svr_model = SVR(kernel="poly", degree=5)
svr_model = SVR(kernel="rbf")
svr_model.fit(X, y)

pred = svr_model.predict(X)

plt.scatter(X, y, color="Blue")
plt.plot(X, pred)
plt.show()

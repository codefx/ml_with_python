#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 00:00:49 2023

@author: cfxm
"""
# =============================================================================
# Additional Information

# This is a transnational data set which contains all the transactions occurring between
# 01/12/2010 and 09/12/2011 for a UK-based and registered non-store online retail.The company
# mainly sells unique all-occasion gifts. Many customers of the company are wholesalers.

# Has Missing Values?   # No
# Variables Table
# Variable Name	Role	Type	Demographic	Description	Units	                               Missing Values
# InvoiceNo	ID	Categorical		a 6-digit integral number uniquely assigned to each transaction.
# If this code starts with letter 'c', it indicates a cancellation		                             no
# StockCode	ID	Categorical		a 5-digit integral number uniquely assigned to each distinct product	 no
# Description	Feature	Categorical		product name		                                              no
# Quantity	Feature	Integer		the quantities of each product (item) per transaction		         no
# InvoiceDate	Feature	Date		the day and time when each transaction was generated		         no
# UnitPrice	Feature	Continuous		product price per unit	sterling	                                 no
# CustomerID	Feature	Categorical		a 5-digit integral number uniquely assigned to each customer		 no
# Country	Feature	Categorical		the name of the country where each customer resides		         no
# =============================================================================
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import datetime as dt
from sklearn.preprocessing import MinMaxScaler
from yellowbrick.cluster import KElbowVisualizer
from sklearn.cluster import KMeans


# data = pd.read_excel("Online_Retail.xlsx")
# data.to_csv("Online_Retail.csv", index=False) #to decrease reading time.
data = pd.read_csv("Online_Retail.csv")
onre_data = data.copy()
# print(onre_data.isnull().sum())
onre_data = onre_data.dropna()
# print(onre_data.isnull().sum())
# print(onre_data.info())
onre_data["Total"] = onre_data["Quantity"]*onre_data["UnitPrice"]
# print(onre_data.head(10))
onre_data = onre_data.drop(onre_data[onre_data["Total"] <= 0].index)
# print(onre_data[onre_data["Total"] <= 0])
# sns.boxplot(onre_data["Total"])
# plt.show()

Q1 = onre_data["Total"].quantile(0.25)
Q3 = onre_data["Total"].quantile(0.75)
IQR = Q3-Q1
lowband = Q1-1.5*IQR
highband = Q3+1.5*IQR
onre_data2 = onre_data[~((onre_data["Total"] > highband)
                         | (onre_data["Total"] < lowband))]
# print(onre_data2.shape)
onre_data2 = onre_data2.reset_index(drop=True)
# print(onre_data2)
# print(len(onre_data2["CustomerID"].unique()))  #4194 unique customer
# print(onre_data2["CustomerID"].nunique())  # 4194 unique customer
# print(onre_data2["InvoiceNo"].nunique())  # 16806 unique invoice
onre_data2["CustomerID"] = onre_data2["CustomerID"].astype("int")
# print(onre_data2["CustomerID"].head(10))
# print(onre_data2.info())
onre_data2["InvoiceDate"] = pd.to_datetime(onre_data2["InvoiceDate"])
# print(onre_data2.info())

# for non dynamic data|| 2011-12-09 12:50:00
todays_date = onre_data2["InvoiceDate"].max()
# print(todays_date)
todays_date = dt.datetime(2011, 12, 9, 12, 50, 0)
# print(todays_date)
r = (todays_date-onre_data2.groupby("CustomerID").agg(
    {"InvoiceDate": "max"})).apply(lambda x: x.dt.days)
# print(r)
f = onre_data2.groupby(["CustomerID", "InvoiceNo"]).agg({"InvoiceNo": "count"})
# print(f)
f2 = f.groupby("CustomerID").agg({"InvoiceNo": "count"})
# print(f2)
m = onre_data2.groupby("CustomerID").agg({"Total": "sum"})
# print(m)
RFM = r.merge(f2, on="CustomerID").merge(m, on="CustomerID")
RFM = RFM.reset_index()
RFM = RFM.rename(columns={"InvoiceDate": "Recency",
                 "InvoiceNo": "Frequency", "Total": "Monetary"})
# print(RFM)
df = RFM.iloc[:, 1:]
# print(df)

df_norm = MinMaxScaler().fit_transform(df)
df_norm = pd.DataFrame(df_norm, columns=df.columns)
# print(df_norm)

kmodel = KMeans(random_state=0)
# graph = KElbowVisualizer(kmodel, k=(2, 10))
# graph.fit(df_norm)
# graph.poof() #k=4
kmodel2 = KMeans(random_state=0, n_clusters=4, init="k-means++")
kfit = kmodel2.fit(df_norm)
labels = kfit.labels_
RFM["Labels"] = labels
# sns.scatterplot(x="Labels", y="CustomerID",
#                 data=RFM, hue=labels, palette="deep")
# plt.xlim([-1, 5])
# plt.show()

# print(RFM.groupby("Labels")["CustomerID"].count())
print(RFM.groupby("Labels").mean().iloc[:, 1:])

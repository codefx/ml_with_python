#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  1 22:34:04 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts

from itertools import islice

data = pd.read_csv("twitter_dataset.csv")
data2 = pd.read_csv("sentiment-emotion-labelled_Dell_tweets.csv")
twit_data = data.copy()
twit_data = twit_data[["Text", "Timestamp"]]
# print(twit_data.shape)  # (10000, 2)
# print(twit_data.isnull().sum())
# print(twit_data.head(5))  # timestamp 2023-01-30 11:00:51
twit_data2 = data2.copy()
df = twit_data2[["Text", "Datetime"]].copy()
# print(twit_data2.shape)  # (24970, 2)
# print(twit_data2.isnull().sum())
# print(df.head(5))  # Datetime 2022-09-30 23:29:15+00:00
df["Datetime"] = pd.to_datetime(df["Datetime"])
# print(df.info())  #datetime64[ns, UTC]
# df["Datetime2"] = df["Datetime"].dt.date


def correct_date(df):
    corr_cols = df.select_dtypes(include=['datetime64[ns, UTC]']).columns
    for i in corr_cols:
        df[i] = df[i].dt.date

    return df


corrected_df = correct_date(df)
print(corrected_df.head(5))
# print(corrected_df.info())
df.to_csv("twitter_corrected_dataset.csv", index=False)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 23:34:08 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, KFold as kf
from sklearn.linear_model import LinearRegression as linreg
import sklearn.metrics as mtr

data = pd.read_csv("advertising.xls")
adv_data = data.copy()
# print(adv_data)

y = adv_data.Sales
X = adv_data.drop(columns="Sales", axis=1)

X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)
lr = linreg()
model = lr.fit(X_train, y_train)


def scores(model, x_trn, x_tst, y_trn, y_tst):
    trn_prdict = model.predict(x_trn)
    tst_prdict = model.predict(x_tst)

    r2_trn = mtr.r2_score(y_trn, trn_prdict)
    r2_test = mtr.r2_score(y_tst, tst_prdict)

    mse_trn = mtr.mean_squared_error(y_trn, trn_prdict)
    mse_tst = mtr.mean_squared_error(y_tst, tst_prdict)

    return [r2_trn, r2_test, mse_trn, mse_tst]


result = scores(lr, X_train, X_test, y_train, y_test)


def scores_print(res=[], iter=False, num=None):
    print("--------------------------------------")
    if iter == True:
        print(f"Iteration : {num}")
        print(f"eğitim R2= {res[0]}  eğitim MSE= {res[2]}")
        print(f"test R2= {res[1]}  test MSE= {res[3]}")
    else:
        print(f"eğitim R2= {res[0]}  eğitim MSE= {res[2]}")
        print(f"test R2= {res[1]}  test MSE= {res[3]}")


print(f"eğitim R2= {result[0]}  eğitim MSE= {result[2]}")
print(f"test R2= {result[1]}  test MSE= {result[3]}")

lr_cv = linreg()
k = 5
itert = 1
cv = kf(n_splits=k)  # cross validation cv
# we r looking higher r2, lower mse

for trn_index, tst_index in cv.split(X):
    X_train, X_test = X.loc[trn_index], X.loc[tst_index]
    y_train, y_test = y.loc[trn_index], y.loc[tst_index]
    lr_cv.fit(X_train, y_train)
    result2 = scores(lr_cv, X_train, X_test, y_train, y_test)
    scores_print(res=result2, iter=True, num=itert)
    if (result2[1] > result[1]) and (result2[3] < result[3]):
        print("this version is good")
    itert += 1

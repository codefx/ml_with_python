#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 14:14:24 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score, classification_report, roc_curve, roc_auc_score


data = pd.read_csv("logreg_data.csv")
logreg_data = data.copy()
# print(logreg_data.isnull().sum())
# print(logreg_data.info())
logreg_data = logreg_data.drop(columns=["id", "Unnamed: 32"], axis=1)
# print(logreg_data.info())
logreg_data.diagnosis = [1 if value ==
                         "M" else 0 for value in logreg_data.diagnosis]
# print(logreg_data)


y = logreg_data.diagnosis
X = logreg_data.drop(columns="diagnosis", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stsc = StandardScaler()
X_train = stsc.fit_transform(X_train)
X_test = stsc.transform(X_test)

model_lor = LogisticRegression(random_state=0)
model_lor.fit(X_train, y_train)

pred = model_lor.predict(X_test)

confmat = confusion_matrix(y_test, pred)
# print(confmat)
accu_score = accuracy_score(y_test, pred)
# print(accu_score)
clsf_report = classification_report(y_test, pred)
# print(clsf_report)
f1score = f1_score(y_test, pred)
# print(f1score)

aucscore = roc_auc_score(y_test, pred)

fpr, tpr, thresold = roc_curve(y_test, model_lor.predict_proba(X_test)[:, 1])
plt.plot(fpr, tpr, label="Model AUC (Alan=%0.2f)" % aucscore)
plt.plot([0, 1], [0, 1], "r--")
plt.xlabel("False Positive Rate")
plt.ylabel("True Positive Rate")
plt.title("ROC")
plt.legend(loc="lower right")
plt.show()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 25 22:57:45 2023

@author: cfxm
"""
from nltk.corpus import stopwords
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts
import re
import nltk
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB  # usually used for text structures
from sklearn.metrics import accuracy_score

# nltk.download("stopwords")

data = pd.read_csv("spam.csv", encoding="utf-8")
spam_data = data.copy()
spam_data = spam_data.drop(
    columns=["Unnamed: 2", "Unnamed: 3", "Unnamed: 4"], axis=1)
spam2_data = spam_data[["v1", "v2"]].copy()
spam2_data = spam2_data.rename(columns={"v1": "Label", "v2": "Sms"})
# spam2_data.to_excel("new_spam_data.xlsx")
# print(spam2_data.info())
# print(spam2_data.groupby("Label").count())
# print(spam2_data.describe())
spam2_data = spam2_data.drop_duplicates()
# print(spam2_data.describe())
# print(spam2_data.isnull().sum())
spam2_data["Char_count"] = spam2_data["Sms"].str.len()
# print(spam2_data.head())

# spam2_data.hist(column="Char_count", by="Label", bins=50)
# plt.show()
spam2_data.Label = [1 if value == "spam" else 0 for value in spam2_data.Label]
# print(spam2_data.head())


def onlyletters(sentence):
    place = re.compile("[^a-zA-Z]")
    return re.sub(place, " ", str(sentence))


# print(onlyletters("lam3 and W01f"))  # lam  and W  f

# tr support included also in the package nltk
# stopping = stopwords.words("english")
# print(stopping)

spams = []
hams = []
all_sentences = []

for i in range(len(spam2_data.Sms.values)):
    res1 = spam2_data.Sms.values[i]
    res2 = spam2_data.Label.values[i]

    cleaned = []
    sentences = onlyletters(res1).lower()

    for words in sentences.split():
        cleaned.append(words)

        if res2 == 1:
            spams.append(sentences)
        else:
            hams.append(sentences)

    all_sentences.append(" ".join(cleaned))


spam2_data["New_Sms"] = all_sentences
# print(spam2_data)
spam3_data = spam2_data.drop(columns=["Sms", "Char_count"])

covect = CountVectorizer()
x = covect.fit_transform(spam3_data.New_Sms).toarray()

y = spam3_data.Label
X = x

X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

model = MultinomialNB()
model.fit(X_train, y_train)
pred = model.predict(X_test)

acc_score = accuracy_score(y_test, pred)
print(acc_score)
print("-------------------")

for i in np.arange(0.0, 1.1, 0.1):
    model2 = MultinomialNB(alpha=i)
    model2.fit(X_train, y_train)
    pred2 = model2.predict(X_test)

    acc_score2 = accuracy_score(y_test, pred2)
    print(f"for alfa {round(i,1)} value, score is: {round(acc_score2*100,2)} ")

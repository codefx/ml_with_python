#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 16:15:49 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from mlxtend.frequent_patterns import apriori, association_rules

data = pd.read_csv("Online_Retail.csv")
onre_data = data.copy()
# print(onre_data.isnull().sum())
onre_data = onre_data.dropna()
# print(onre_data.isnull().sum())
# print(onre_data.info())
onre_data = onre_data[~onre_data["InvoiceNo"].str.contains("C")]
# print(onre_data.shape)
# onre_data2 = onre_data["Country"].value_counts()
# print(onre_data2)  #United Kingdom          354345
country = onre_data[onre_data["Country"] == "United Kingdom"]
basket = country.iloc[:, [0, 2, 3]]
# print(basket)
basket2 = basket.groupby(["InvoiceNo", "Description"])[
    "Quantity"].sum()  # to stack dataframe
basket3 = basket.groupby(["InvoiceNo", "Description"])[
    "Quantity"].sum().unstack().reset_index()  # to unstack
# print(basket3)

basket4 = basket.groupby(["InvoiceNo", "Description"])[
    "Quantity"].sum().unstack().reset_index().fillna(0).set_index("InvoiceNo")
# print(basket4)


def numto1(x):
    if x <= 0:
        return 0
    if x >= 1:
        return 1


basket5 = basket4.map(numto1)
# print(basket5)
df1 = apriori(basket5.astype("bool"), min_support=0.02, use_colnames=True)
# df2 = association_rules(df1, metric="confidence")
df2 = association_rules(df1, metric="lift", min_threshold=1)
print(df2)
# print(df2.iloc[0])

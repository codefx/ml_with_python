#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  1 19:25:28 2023

@author: cfxm
"""
import matplotlib.pyplot as plt
import re
import string
import pandas as pd
# from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords
import nltk
nltk.download("stopwords")
nltk.download("wordnet")
# for further correction, like word "crazy" not to be  "crazi"
lemma = nltk.WordNetLemmatizer()

data = pd.read_excel("new_spam_data.xlsx")
spam_data = data.copy()
spam_data = spam_data.dropna()
spam_data = spam_data.drop(columns=["Unnamed: 0"], axis=1)
spam_data = spam_data.reset_index()


def cleanWords(df, colname, lang=""):
    cleaned = []
    dfc = df[colname]
    lang = "english" if lang == "" else lang
    for ix in range(len(df)):
        # if u use re  index is important.  (df=df.reset_index())
        modified = re.sub('[^a-zA-Z]', ' ', dfc[ix])
        modified = modified.lower()
        modified = modified.split()
        modified = [lemma.lemmatize(word)  # only here different
                    for word in modified if not word in set(stopwords.words(lang))]
        modified = ' '.join(modified)
        cleaned.append(modified)

    return cleaned


cleaned = cleanWords(df=spam_data, colname="Sms")
# print(cleaned[20])
df = pd.DataFrame(list(zip(spam_data["Sms"], cleaned)), columns=[
                  "Original_Sms", "Cleaned_Sms"])
# print(cleaned[0])
freq = (df["Cleaned_Sms"]).apply(lambda x: pd.value_counts(
    x.split(" "))).sum(axis=0).reset_index()
freq.columns = ["Words", "Frequency"]
# print(freq)
# print(freq.info())
# print(freq.nunique())
filterq = freq[freq["Frequency"] > 250]
# plt.bar(x="Words", height="Frequency", data=freq)
filterq.plot.bar(x="Words", y="Frequency")
plt.show()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 15:04:34 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.tree import plot_tree


data = pd.read_csv("diabetes.xls")
diab_data = data.copy()
# print(diab_data.info())
# print(diab_data.isnull().sum())

y = diab_data.Outcome
X = diab_data.drop(columns="Outcome", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stasca = StandardScaler()

X_train = stasca.fit_transform(X_train)
X_test = stasca.transform(X_test)


ran_for_clsf_model = RandomForestClassifier(random_state=0)
ran_for_clsf_model.fit(X_train, y_train)
pred = ran_for_clsf_model.predict(X_test)

accu_score = accuracy_score(y_test, pred)
print(accu_score)  # 0.7662337662337663


# parameters = {"criterion": ["gini", "entropy", "log_loss"],
#               "max_depth": range(2, 10),
#               "min_samples_split": range(2, 10),
#               "n_estimators": [1000, 1500, 2000]}

# grid = GridSearchCV(ran_for_clsf_model,
#                     param_grid=parameters, cv=10, n_jobs=-1)
# grid.fit(X_train, y_train)
# print(grid.best_params_)
# {'criterion': 'entropy', 'max_depth': 10, 'min_samples_split': 5, 'n_estimators': 1000}
# {'criterion': 'entropy', 'max_depth': 9, 'min_samples_split': 3, 'n_estimators': 1000}

ran_for_clsf_model2 = RandomForestClassifier(random_state=0, criterion="entropy",
                                             max_depth=5, min_samples_split=5,
                                             n_estimators=1000)
ran_for_clsf_model2.fit(X_train, y_train)
pred2 = ran_for_clsf_model2.predict(X_test)

accu_score2 = accuracy_score(y_test, pred2)
print(accu_score2)  # 0.7727272727272727


# plot_tree(ran_for_clsf_model2[0], filled=True, fontsize=7)

importance = pd.DataFrame(
    {"Importance": ran_for_clsf_model2.feature_importances_}, index=X.columns)
importance.sort_values(by="Importance", axis=0, ascending=True).plot(
    kind="barh", color="blue")
plt.title("Variables's Importance Levels")

plt.show()

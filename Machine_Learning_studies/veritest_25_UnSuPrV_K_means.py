#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 29 11:33:08 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
# from sklearn.model_selection import train_test_split as tts
from sklearn.cluster import KMeans
from yellowbrick.cluster import KElbowVisualizer

data = pd.read_csv("Mall_Customers.xls")
cust_data = data.copy()
# print(data)
cust_data = cust_data.drop(columns="CustomerID", axis=1)
# print(cust_data.isnull().sum())
# print(cust_data.info())
# print(cust_data.describe())
X = cust_data.iloc[:, 1:3]
# print(X)
# plt.scatter(X.iloc[:, 0], X.iloc[:, 1], color="blue")
# plt.show()

# kmodel = KMeans(n_clusters=2, random_state=0)
# kmod_fit = kmodel.fit(X)
# # print(kmodel.cluster_centers_)
# clusts = kmod_fit.labels_
# centrs = kmod_fit.cluster_centers_
# figure, axis = plt.subplots(1, 2)
# axis[0].scatter(X.iloc[:, 0], X.iloc[:, 1], color="black")
# axis[1].scatter(X.iloc[:, 0], X.iloc[:, 1], c=clusts, cmap="winter")
# axis[1].scatter(centrs[:, 0], centrs[:, 1],  c="red", s=200)

# plt.show()

# kmodel2 = KMeans(random_state=0)
# graph = KElbowVisualizer(kmodel2, k=(1, 20))
# graph.fit(X)
# graph.poof()  # k=4

kmodel = KMeans(n_clusters=4, random_state=0)
kmod_fit = kmodel.fit(X)
# print(kmodel.cluster_centers_)
clusts = kmod_fit.labels_
centrs = kmod_fit.cluster_centers_
figure, axis = plt.subplots(1, 2)
axis[0].scatter(X.iloc[:, 0], X.iloc[:, 1], color="black")
axis[1].scatter(X.iloc[:, 0], X.iloc[:, 1], c=clusts, cmap="winter")
axis[1].scatter(centrs[:, 0], centrs[:, 1],  c="red", s=200)

plt.show()

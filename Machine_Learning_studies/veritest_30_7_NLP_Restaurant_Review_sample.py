#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  1 21:40:24 2023

@author: cfxm
"""
from nltk.corpus import stopwords
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split as tts
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
import re
import nltk
nltk.download("stopwords")
nltk.download("wordnet")
lemma = nltk.WordNetLemmatizer()
data = pd.read_csv("Restaurant_Reviews.tsv", delimiter="\t")
rest_rev_data = data.copy()

# print(rest_rev_data.isnull().sum())


def cleanWords(df, colname, lang=""):
    cleaned = []
    dfc = df[colname]
    lang = "english" if lang == "" else lang
    for ix in range(len(df)):
        # if u use re  index is important.  (df=df.reset_index())
        modified = re.sub('[^a-zA-Z]', ' ', dfc[ix])
        modified = modified.lower()
        modified = modified.split()
        modified = [lemma.lemmatize(word)  # only here different
                    for word in modified if not word in set(stopwords.words(lang))]
        modified = ' '.join(modified)
        cleaned.append(modified)

    return cleaned


cleaned = cleanWords(rest_rev_data, "Review")
# print(cleaned)

# df = pd.DataFrame(list(zip(rest_rev_data["Review"], cleaned)), columns=[
#                   "Original_Review", "Cleaned_Review"])
# # print(df)
# freq = (df["Cleaned_Review"]).apply(
#     lambda x: pd.value_counts(x.split(" "))).sum(axis=0).reset_index()
# freq.columns = ["Words", "Frequency"]
# # print(freq)
# filterFreq = freq[freq["Frequency"] > 10]
# filterFreq.plot.bar(x="Words", y="Frequency")
# plt.show()

cnt_Vec = CountVectorizer(max_features=1500)
matrix = cnt_Vec.fit_transform(cleaned).toarray()
# matrixdf = pd.DataFrame(matrix, columns=cnt_Vec.get_feature_names_out())
# print(matrixdf)
# print(rest_rev_data["Liked"].unique())
# print(rest_rev_data.iloc[:, 1].values)
y = rest_rev_data.iloc[:, 1].values
X_train, X_test, y_train, y_test = tts(
    matrix, y, test_size=0.2, random_state=42)

model = GaussianNB()
model.fit(X_train, y_train)
pred = model.predict(X_test)
acc_score = accuracy_score(y_test, pred)
print(acc_score*100)  # 68.0
model2 = RandomForestClassifier(random_state=0)
model2.fit(X_train, y_train)
pred2 = model2.predict(X_test)
acc_score2 = accuracy_score(y_test, pred2)
print(acc_score2*100)  # 74.5

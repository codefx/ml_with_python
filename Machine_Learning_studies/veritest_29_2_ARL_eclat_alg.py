#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 15:51:35 2023

@author: cfxm
"""
# =============================================================================
#   Association Rule Learning ARL
# =============================================================================
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from pyECLAT import ECLAT

data = pd.read_csv("GroceryStoreDataSet.xls", header=None)
grocery_data = data.copy()
grocery_data.columns = ["Product"]
# print(grocery_data)
grocery_data2 = list(grocery_data["Product"].apply(lambda x: x.split(",")))
# print(grocery_data2)
grocery_data3 = pd.DataFrame(grocery_data2)
# print(grocery_data3)

min_product = 2
min_support = 0.2
max_product = max([len(x) for x in grocery_data2])
# print(max_product) #  4

ecl = ECLAT(grocery_data3, verbose=True)

a, b = ecl.fit(min_support=min_support,
               min_combination=min_product, max_combination=max_product)
print(b)

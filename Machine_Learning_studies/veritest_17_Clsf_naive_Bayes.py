#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 25 22:47:11 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score


data = pd.read_csv("cancerdata.csv")  # same as logreg_data
cancer_data = data.copy()
# print(cancer_data.isnull().sum())
# print(cancer_data.head())
# print(cancer_data.info())
cancer_data = cancer_data.drop(columns=["id", "Unnamed: 32"], axis=1)
# print(cancer_data.info())
cancer_data.diagnosis = [1 if value ==
                         "M" else 0 for value in cancer_data.diagnosis]


y = cancer_data.diagnosis
X = cancer_data.drop(columns="diagnosis", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stsc = StandardScaler()
X_train = stsc.fit_transform(X_train)
X_test = stsc.transform(X_test)

gnb_model = GaussianNB()
gnb_model.fit(X_train, y_train)
pred = gnb_model.predict(X_test)

accu_score = accuracy_score(y_test, pred)
print(accu_score)

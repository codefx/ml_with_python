#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 14:08:51 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score

# =============================================================================
#   source: kaggle.com /  Mehmet Akturk's diabetes dataset
#   data description,
# Pregnancies: Number of times pregnant
# Glucose: Plasma glucose concentration a 2 hours in an oral glucose tolerance test
# BloodPressure: Diastolic blood pressure (mm Hg)
# SkinThickness: Triceps skin fold thickness (mm)
# Insulin: 2-Hour serum insulin (mu U/ml)
# BMI: Body mass index (weight in kg/(height in m)^2)
# DiabetesPedigreeFunction: Diabetes pedigree function
# Age: Age (years)
# Outcome: Class variable (0 or 1)
# =============================================================================

data = pd.read_csv("diabetes.xls")
diab_data = data.copy()
# print(diab_data.info())
# print(diab_data.isnull().sum())

y = diab_data.Outcome
X = diab_data.drop(columns="Outcome", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stasca = StandardScaler()

X_train = stasca.fit_transform(X_train)
X_test = stasca.transform(X_test)


def get_models_score(model):
    model.fit(X_train, y_train)
    pred = model.predict(X_test)
    score = accuracy_score(y_test, pred)
    return round(score*100, 2)


# print(get_models_score(DecisionTreeClassifier()))
models = []
models.append(("Logistic Regression", LogisticRegression(random_state=0)))
models.append(("K N N", KNeighborsClassifier()))
models.append(("S V C", SVC(random_state=0)))
models.append(("Bayes", GaussianNB()))
models.append(("DecisionTree Classifier",
              DecisionTreeClassifier(random_state=0)))

modelname = []
accuracy = []
for i in models:
    modelname.append(i[0])
    accuracy.append(get_models_score(i[1]))

a = list(zip(modelname, accuracy))
result = pd.DataFrame(a, columns=["Model_Name", "Score"])

print(result)
# =============================================================================
# todo:  for hyper parameters write gridcv
# =============================================================================

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 23:40:29 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score


data = pd.read_csv("cancerdata.csv")  # same as logreg_data
cancer_data = data.copy()
# print(cancer_data.isnull().sum())
# print(cancer_data.head())
# print(cancer_data.info())
cancer_data = cancer_data.drop(columns=["id", "Unnamed: 32"], axis=1)
# print(cancer_data.info())
cancer_data.diagnosis = [1 if value ==
                         "M" else 0 for value in cancer_data.diagnosis]


y = cancer_data.diagnosis
X = cancer_data.drop(columns="diagnosis", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stsc = StandardScaler()
X_train = stsc.fit_transform(X_train)
X_test = stsc.transform(X_test)

svm_model = SVC(random_state=0)
svm_model.fit(X_train, y_train)
pred = svm_model.predict(X_test)


accu_score = accuracy_score(y_test, pred)
print(accu_score)

parameters = {"C": range(1, 20), "kernel": [
    "linear", "poly", "rbf", "sigmoid"]}
grid = GridSearchCV(estimator=svm_model,
                    param_grid=parameters, cv=10, n_jobs=-1)
grid.fit(X_train, y_train)

print(grid.best_params_)  # {'C': 2, 'kernel': 'rbf'}

svm_model2 = SVC(random_state=0, C=2)  # default rbf
svm_model2.fit(X_train, y_train)
pred2 = svm_model2.predict(X_test)


accu_score2 = accuracy_score(y_test, pred2)
print(accu_score2)

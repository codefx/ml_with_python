#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  1 11:53:00 2023

@author: cfxm
"""
import matplotlib.pyplot as plt
import re
import string
import pandas as pd
from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords
import nltk
nltk.download("stopwords")
nltk.download("wordnet")
lemma = nltk.WordNetLemmatizer()
# import numpy as np
# import seaborn as sns


data = pd.read_excel("new_spam_data.xlsx")
spam_data = data.copy()
spam_data = spam_data.dropna()
spam_data = spam_data.drop(columns=["Unnamed: 0"], axis=1)


# print(spam_data)
spam_data = spam_data.reset_index()
# print(spam_data)
# print(spam_data.isnull().sum())
# spam_data2 = spam_data.copy()
# spam_data2 = spam_data["Sms"].str.replace(
#     '[^\w\s]', '', regex=True).str.lower()

# print(spam_data)

# spam_data3 = re.sub('[^a-zA-Z]', " ", spam_data2["Sms"][0]).lower()

# print(spam_data3.lower())


# # print(stopwords.fileids())
# impotent = stopwords.words("english")
# # print(impotent)
# # wsplit = spam_data2.str.split()
# # print(wsplit)
# wsplit_join = spam_data2.apply(lambda x: " ".join(
#     x for x in x.split() if x not in impotent))
# # print(impotent)
# # print(wsplit_join)
# porstm = PorterStemmer()
# print(porstm.stem("waiting"))

def cleanWords(df, colname, lang=""):
    cleaned = []
    ps = PorterStemmer()
    dfc = df[colname]
    lang = "english" if lang == "" else lang

    for ix in range(len(df)):
        # if u use re  index is important.  (df=df.reset_index())
        modified = re.sub('[^a-zA-Z]', ' ', dfc[ix])
        modified = modified.lower()
        modified = modified.split()
        modified = [ps.stem(word)
                    for word in modified if not word in set(stopwords.words(lang))]
        modified = ' '.join(modified)
        cleaned.append(modified)

    return cleaned


cleaned = cleanWords(df=spam_data, colname="Sms")
# print(cleaned[20])
df = pd.DataFrame(list(zip(spam_data["Sms"], cleaned)), columns=[
                  "Original_Sms", "Cleaned_Sms"])
print(df)
df.to_csv("cleaned_spam_sms.csv")

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 16:16:25 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, GridSearchCV
from xgboost import XGBClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import GaussianNB

data = pd.read_csv("diabetes.xls")
diab_data = data.copy()
# print(diab_data.info())
# print(diab_data.isnull().sum())

y = diab_data.Outcome
X = diab_data.drop(columns="Outcome", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stasca = StandardScaler()

X_train = stasca.fit_transform(X_train)
X_test = stasca.transform(X_test)

model_xgb = XGBClassifier()
model_xgb.fit(X_train, y_train)
predXgb = model_xgb.predict(X_test)

acc_score = accuracy_score(y_test, predXgb)
print(acc_score*100)  # 70.77922077922078


# model_bayes = GaussianNB()
# model_bayes.fit(X_train, y_train)
# predbayes = model_bayes.predict(X_test)

# acc_score2 = accuracy_score(y_test, predbayes)
# print(acc_score2*100)  # 76.62337662337663

# parameters = {"max_depth": [3, 5, 7], "subsample": [0.2, 0.5, 0.7],
#               "n_estimators": [500, 1000, 2000], "learning_rate": [0.2, 0.5, 0.7]}

# grid = GridSearchCV(model_xgb, param_grid=parameters, cv=10, n_jobs=-1)
# grid.fit(X_train, y_train)

# print(grid.best_params_)
# # {'learning_rate': 0.2, 'max_depth': 7, 'n_estimators': 500, 'subsample': 0.7}

model_xgb2 = XGBClassifier(
    learning_rate=0.2, max_depth=10, n_estimators=500, subsample=0.7)
model_xgb2.fit(X_train, y_train)
predXgb2 = model_xgb2.predict(X_test)

acc_score3 = accuracy_score(y_test, predXgb2)
print(acc_score3*100)  # 71.42857142857143  #72.07792207792207

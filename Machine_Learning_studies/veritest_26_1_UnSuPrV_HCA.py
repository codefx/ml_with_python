#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 29 12:37:14 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.cluster import AgglomerativeClustering
from scipy.cluster.hierarchy import dendrogram, linkage

# =============================================================================
# hierarchical clustering (also called hierarchical cluster analysis or HCA)
# 1-Agglomerative: This is a "bottom-up" approach: Each observation starts in its own cluster,
#            and pairs of clusters are    merged as one moves up the hierarchy.
# 2-Divisive: This is a “top-down” approach: All observations start in one cluster, and splits
#           are performed recursively as one moves down the hierarchy.
# =============================================================================


data = pd.read_csv("Mall_Customers.xls")
cust_data = data.copy()
# print(data)

X = cust_data.iloc[:, 2:4]
# print(X)

# n_clusters= default is  2. 2 clusters 1 and 0
model_hca_agg = AgglomerativeClustering()
pred = model_hca_agg.fit_predict(X)
# print(pred)
X["Clust_Pred"] = pred
# print(X)
# print(X["Age"][X["Clust_Pred"] == 0])

plt.scatter(X["Age"][X["Clust_Pred"] == 0],
            X["Annual Income (k$)"][X["Clust_Pred"] == 0], c="red")
plt.scatter(X["Age"][X["Clust_Pred"] == 1],
            X["Annual Income (k$)"][X["Clust_Pred"] == 1], c="green")
# plt.show()

link = linkage(X)
dendrogram(link)
plt.xlabel("Data points")
plt.ylabel("Distance")
plt.show()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 21:24:59 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, GridSearchCV
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import BaggingRegressor, RandomForestRegressor
import sklearn.metrics as mtr
import xgboost as xgb


# =====================Random Forest=======================================
# RandomForest oluşturulan alt kumeleri(bagging deki bootstraplar) dikey olarak
# da parçalar, yani bazı X leri alır bazılarını almaz,ba.sız değişkenlerin
#   etkilerini de açığa  çıkarır bu yüzden daha güçlüdür.
# =============================================================================


data = pd.read_csv("advertising.xls")
adv_data = data.copy()


y = adv_data.Sales
X = adv_data.drop(columns="Sales", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)
# =============================================================================
# decision tree regression section
# =============================================================================
dtr_model = DecisionTreeRegressor(
    random_state=0, max_leaf_nodes=18, min_samples_split=4)
dtr_model.fit(X_train, y_train)
dtr_pred = dtr_model.predict(X_test)

r2 = mtr.r2_score(y_test, dtr_pred)
rmse = mtr.mean_squared_error(y_test, dtr_pred, squared=False)

print(f"Decision Tree Reg -> R2 : {r2}  RMSE : {rmse}")

# =============================================================================
# bagging decision tree regression section
# =============================================================================
bg_model = BaggingRegressor(random_state=0, n_estimators=23)
bg_model.fit(X_train, y_train)
bg_pred = bg_model.predict(X_test)

r2_bg = mtr.r2_score(y_test, bg_pred)
rmse_bg = mtr.mean_squared_error(y_test, bg_pred, squared=False)

print(f"Bagging T Reg-> R2 : {r2_bg}  RMSE : {rmse_bg}")

# =============================================================================
# random forest regression section
# =============================================================================
rf_model = RandomForestRegressor(
    random_state=0, max_depth=8, max_features=2, n_estimators=16)
rf_model.fit(X_train, y_train)
rf_pred = rf_model.predict(X_test)

r2_rf = mtr.r2_score(y_test, rf_pred)
rmse_rf = mtr.mean_squared_error(y_test, rf_pred, squared=False)

print(f"Random forest T Reg-> R2 : {r2_rf}  RMSE : {rmse_rf}")

# =============================================================================
# grid section  / Tuning section / Cross Validation / for hyper parameters
# =============================================================================
# parameters1 = {"min_samples_split": range(
#     2, 25), "max_leaf_nodes": range(2, 25)}
# grid1 = GridSearchCV(estimator=dtr_model, param_grid=parameters1, cv=10)
# grid1.fit(X_train, y_train)
# print(grid1.best_params_)  # {'max_leaf_nodes': 18, 'min_samples_split': 4}

# parameters2 = {"n_estimators": range(2, 25)}
# grid2 = GridSearchCV(estimator=bg_model, param_grid=parameters2, cv=10)
# grid2.fit(X_train, y_train)
# print(grid2.best_params_)  # {'n_estimators': 23}

# parameters3 = {"max_depth": range(2, 25), "max_features": range(
#     2, 25), "n_estimators": range(2, 25)}
# grid3 = GridSearchCV(estimator=rf_model,
#                      param_grid=parameters3, cv=10, n_jobs=-1)
# grid3.fit(X_train, y_train)
# print(grid3.best_params_)  #{'max_depth': 8, 'max_features': 2, 'n_estimators': 16}

# parameterX = {"n_estimators": [100, 200, 300],
#               "learning_rate": [0.1, 0.01, 0.001],
#               "max_depth": range(2, 25),
#               "subsample": [0.8],
#               "colsample_bytree": [0.8],
#               "objective": ['reg:squarederror']
#               }
# gridx = GridSearchCV(estimator=xgb_model, param_grid=parameterX, cv=10)
# gridx.fit(X_train, y_train)
#
# # {'colsample_bytree': 0, 'learning_rate': 0.1, 'max_depth': 2, 'n_estimators': 100, 'objective': 'reg:squarederror', 'subsample': 0}
# print(gridx.best_params_)
# =============================================================================
# XGB section
# =============================================================================
# xgb_model = xgb.XGBRFRegressor(
#     random_state=0, learning_rate=0.01, max_depth=10, n_estimators=20)
# xgb_model.fit(X_train, y_train)
# xgb_pred = xgb_model.predict(X_test)
# r2_xgb = mtr.r2_score(y_test, xgb_pred)
# rmse_xgb = mtr.mean_squared_error(y_test, xgb_pred, squared=False)

# print(f"XGB Reg-> R2 : {r2_xgb}  RMSE : {rmse_xgb}")

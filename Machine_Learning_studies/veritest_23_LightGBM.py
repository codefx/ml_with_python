#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 20:48:55 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from lightgbm import LGBMClassifier

data = pd.read_csv("diabetes.xls")
diab_data = data.copy()
# print(diab_data.info())
# print(diab_data.isnull().sum())

y = diab_data.Outcome
X = diab_data.drop(columns="Outcome", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stasca = StandardScaler()

X_train = stasca.fit_transform(X_train)
X_test = stasca.transform(X_test)

model_lgbm = LGBMClassifier()
model_lgbm.fit(X_train, y_train)
predLGBM = model_lgbm.predict(X_test)

acc_score = accuracy_score(y_test, predLGBM)
print(acc_score*100)  # 70.77922077922078

parameters = {"max_depth": [3, 5, 7], "subsample": [0.6, 0.8, 1.0],
              "n_estimators": [200, 500, 1000], "learning_rate": [0.001, 0.01, 0.1]}

# grid = GridSearchCV(model_lgbm, param_grid=parameters, cv=10, n_jobs=-1)
# grid.fit(X_train, y_train)

# print(grid.best_params_)  not working
# {'learning_rate': 0.01, ‘max_depth': 3, "n_estimators': 1000, ‘subsample’: 0.6} nrtr

model_lgbm2 = LGBMClassifier(
    learning_rate=0.01, max_depth=3, n_estimators=1000, subsample=0.6)
model_lgbm2.fit(X_train, y_train)
predLGBM2 = model_lgbm2.predict(X_test)

acc_score2 = accuracy_score(y_test, predLGBM2)
print(acc_score2*100)  # 75.32467532467533

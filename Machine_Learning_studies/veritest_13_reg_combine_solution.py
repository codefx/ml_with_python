#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 13:40:01 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts
from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
import sklearn.metrics as mtr


data = pd.read_csv("advertising.xls")
adv_data = data.copy()


y = adv_data.Sales
X = adv_data.drop(columns="Sales", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.1, random_state=42)


def model_pred(model):
    model.fit(X_train, y_train)
    pred = model.predict(X_test)
    return pred


# print(model_pred(LinearRegression()))


def model_pred_only_scores(model):
    model.fit(X_train, y_train)
    pred = model.predict(X_test)
    r2 = mtr.r2_score(y_test, pred)
    rmse = mtr.mean_squared_error(y_test, pred, squared=False)
    return [r2, rmse]


# print(model_pred_with_score(LinearRegression()))

models = [LinearRegression(), Ridge(), Lasso(), ElasticNet(),
          SVR(), DecisionTreeRegressor(random_state=0), BaggingRegressor(random_state=0), RandomForestRegressor(random_state=0)]

names = ["Linear Model", "Ridge Model", "Lasso Model", "ElasticNet Model",
         "SVR Model", "Decision Tree Model", "Bag Model", "Random Forest Model"]

result = []
for i in models:
    result.append(model_pred_only_scores(i))

df = pd.DataFrame(names, columns=["Model Name"])
df2 = pd.DataFrame(result, columns=["R2", "RMSE"])
df = df.join(df2)
print(df)

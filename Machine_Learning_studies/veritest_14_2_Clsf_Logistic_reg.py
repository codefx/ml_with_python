#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 14:14:24 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, cross_val_score
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score


data = pd.read_csv("Iris.csv")
iris_data = data.copy()
# print(iris_data.isnull().sum())
# print(iris_data.info())
iris_data = iris_data.drop(columns="Id", axis=1)
# print(iris_data.info())
print(iris_data.Species.unique())
# print(len(iris_data.Species.unique()))

labenc = LabelEncoder()
iris_data["Species"] = labenc.fit_transform(iris_data["Species"])
print(iris_data.Species.unique())
y = iris_data["Species"]
X = iris_data.drop(columns="Species", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stsc = StandardScaler()
X_train = stsc.fit_transform(X_train)
X_test = stsc.transform(X_test)

model_iris_loreg = LogisticRegression(random_state=0)
model_iris_loreg.fit(X_train, y_train)

pred = model_iris_loreg.predict(X_test)

confmat = confusion_matrix(y_test, pred)
print(confmat)
acc_score = accuracy_score(y_test, pred)
print(acc_score)

cvs = cross_val_score(model_iris_loreg, X_test, y_test,
                      cv=9).mean()  # why not 10 ?
# UserWarning: The least populated class in y has only 9 members,
# which is less than n_splits=10.
print(cvs)

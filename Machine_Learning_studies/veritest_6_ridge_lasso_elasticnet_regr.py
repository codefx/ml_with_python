#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 12:46:07 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts
from sklearn.linear_model import Ridge, Lasso, ElasticNet, ElasticNetCV
# from sklearn.datasets import fetch_california_housing
import sklearn.metrics as mtr


# housing = fetch_california_housing()
# # print(housing)


# df = housing
# data = pd.DataFrame(df.data, columns=df.feature_names)
# house_data["PRICE"] = df.target
# # house_data.to_excel("housing_california.xlsx")
# # print(house_data)
data = pd.read_excel("housing_california.xlsx")
house_data = data.copy()

house_data.dropna()
h_data = house_data.copy()

y = h_data["PRICE"]
X = h_data.drop(columns="PRICE", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)


ridge_model = Ridge(alpha=0.1)
ridge_model.fit(X_train, y_train)

lasso_model = Lasso(alpha=0.1)
lasso_model.fit(X_train, y_train)

elas_model = ElasticNet(alpha=0.1)
elas_model.fit(X_train, y_train)

print(ridge_model.score(X_train, y_train))
print(lasso_model.score(X_train, y_train))
print(elas_model.score(X_train, y_train))
print("---------up train data scores---------")
print("---------down test data scores---------")
print(ridge_model.score(X_test, y_test))
print(lasso_model.score(X_test, y_test))
print(elas_model.score(X_test, y_test))

pred_ridge = ridge_model.predict(X_test)
pred_lasso = lasso_model.predict(X_test)
pred_elas = elas_model.predict(X_test)

print("---mse------")
print(mtr.mean_squared_error(y_test, pred_ridge))
print(mtr.mean_squared_error(y_test, pred_lasso))
print(mtr.mean_squared_error(y_test, pred_elas))

lambdaElas = ElasticNetCV(cv=10, max_iter=10000).fit(X_train, y_train).alpha_
elas_model2 = ElasticNet(alpha=lambdaElas)
elas_model2.fit(X_train, y_train)
print("---after founded lambda elas model 2, train and test, predict")
print(elas_model2.score(X_train, y_train))
print(elas_model2.score(X_test, y_test))

pred_elas2 = elas_model2.predict(X_test)
print(mtr.mean_squared_error(y_test, pred_elas2))

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 30 13:53:48 2023

@author: cfxm
"""

# =============================================================================
#   Association Rule Learning ARL
# =============================================================================
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from mlxtend.preprocessing import TransactionEncoder
from mlxtend.frequent_patterns import apriori, association_rules


data = pd.read_csv("GroceryStoreDataSet.xls", header=None)
grocery_data = data.copy()
grocery_data.columns = ["Product"]
# print(grocery_data)
grocery_data2 = list(grocery_data["Product"].apply(lambda x: x.split(",")))
# print(grocery_data2)
tenc = TransactionEncoder()
tenc_data = tenc.fit(grocery_data2).transform(grocery_data2)
# print(tenc_data)
encoded_data = pd.DataFrame(tenc_data, columns=tenc.columns_)
# print(encoded_data)

df1 = apriori(encoded_data, min_support=0.05, use_colnames=True)
# print(df1)

df2 = association_rules(df1, metric="confidence", min_threshold=0.5)
print(df2)

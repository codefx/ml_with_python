#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  1 11:53:00 2023

@author: cfxm
"""
from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords
import pandas as pd
import string
import re
import matplotlib.pyplot as plt
# import numpy as np
# import seaborn as sns
import nltk
# nltk.download("stopwords")

data = pd.read_excel("new_spam_data.xlsx")
spam_data = data.copy()
spam_data = spam_data.dropna()


# print(spam_data)
# print(spam_data.isnull().sum())
# spam_data2 = spam_data.copy()
spam_data2 = spam_data["Sms"].str.replace(
    '[^\w\s]', '', regex=True).str.lower()

# print(spam_data2)

# spam_data3 = re.sub('[^a-zA-Z]', " ", spam_data2["Sms"][0]).lower()

# print(spam_data3.lower())


# print(stopwords.fileids())
impotent = stopwords.words("english")
# print(impotent)
# wsplit = spam_data2.str.split()
# print(wsplit)
wsplit_join = spam_data2.apply(lambda x: " ".join(
    x for x in x.split() if x not in impotent))
# print(impotent)
# print(wsplit_join)
porstm = PorterStemmer()
print(porstm.stem("waiting"))

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 11:18:58 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts
from sklearn.preprocessing import PolynomialFeatures as polf
from sklearn.linear_model import LinearRegression as linreg
import sklearn.metrics as mtr


data = pd.read_csv("Real estate.csv")
rees_data = data.copy()
# print(rees_data.info())
re_data = rees_data.copy()
re_data.drop(columns=["No", "X1 transaction date",
             "X5 latitude", "X6 longitude"], axis=1, inplace=True)

# print(rees_data)
# print(re_data)
re_data = re_data.rename(columns={"X2 house age": "house_age", "X3 distance to the nearest MRT station": "dist_statn",
                                  "X4 number of convenience stores": "store_nums", "Y house price of unit area": "price"})
# print(re_data.head())
# sns.pairplot(re_data)
# plt.show()
y = re_data.price
X = re_data.drop(columns="price", axis=1)
# print(X)
pol = polf(degree=3)
X_pol = pol.fit_transform(X)
X_train, X_test, y_train, y_test = tts(
    X_pol, y, test_size=0.2, random_state=42)

pol_reg = linreg()
pol_reg.fit(X_train, y_train)
predict = pol_reg.predict(X_test)

r2 = mtr.r2_score(y_test, predict)
mse = mtr.mean_squared_error(y_test, predict)
print(f"r2 : {r2}  MSE : {mse}")

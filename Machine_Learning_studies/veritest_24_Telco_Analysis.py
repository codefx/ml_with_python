#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 17:07:22 2023

@author: cfxm
"""
# TelcoCustomerChurn.xls
# =============================================================================
#  About this file
# Telcom Customer Churn
# Each row represents a customer, each column contains customer’s attributes described on the column Metadata.
# The raw data contains 7043 rows (customers) and 21 columns (features).
# Context

# "Predict behavior to retain customers. You can analyze all relevant customer data and develop focused customer retention programs." [IBM Sample Data Sets]
# Content

# Each row represents a customer, each column contains customer’s attributes described on the column Metadata.
# The data set includes information about:
#     Customers who left within the last month – the column is called Churn
#     Services that each customer has signed up for – phone, multiple lines, internet, online security, online backup, device protection, tech support, and streaming TV and movies
#     Customer account information – how long they’ve been a customer, contract, payment method, paperless billing, monthly charges, and total charges
#     Demographic info about customers – gender, age range, and if they have partners and dependents
# Inspiration

# To explore this type of models and learn more about the subject.
# New version from IBM:
# https://community.ibm.com/community/user/businessanalytics/blogs/steven-macko/2019/07/11/telco-customer-churn-1113

# The “Churn” column is our target.
# =============================================================================
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, GridSearchCV
from sklearn.preprocessing import StandardScaler, LabelEncoder
from lazypredict.Supervised import LazyClassifier
from sklearn.svm import LinearSVC, SVC
from sklearn.linear_model import RidgeClassifier, LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from lightgbm import LGBMClassifier
from xgboost import XGBClassifier
from sklearn.metrics import accuracy_score


data = pd.read_csv("TelcoCustomerChurn.xls")
telco_data = data.copy()
# print(telco_data.shape)
# print(telco_data.info())
telco_data = telco_data.drop(columns="customerID", axis=1)
telco_data["TotalCharges"] = pd.to_numeric(
    telco_data["TotalCharges"], errors="coerce")
telco_data["SeniorCitizen"] = telco_data["SeniorCitizen"].astype(object)


telco_data = telco_data.dropna()
# print(telco_data.isnull().sum())
# print(telco_data.describe())
# print(telco_data.info())
# print(telco_data.select_dtypes(include="object").columns)
# print(telco_data.head())
labenc = LabelEncoder()
variable = telco_data.select_dtypes(include="object").columns

telco_data.update(telco_data[variable].apply(labenc.fit_transform))
# print(telco_data.head())
y = telco_data.Churn
y = y.astype('int')
X = telco_data.drop(columns="Churn", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stasca = StandardScaler()

X_train = stasca.fit_transform(X_train)
X_test = stasca.transform(X_test)

# clf = LazyClassifier()
# #
# models, predicts = clf.fit(X_train, X_test, y_train, y_test)
# queue = models.sort_values(by="Accuracy", ascending=False)
# plt.barh(queue.index, queue["Accuracy"])
# plt.show()

multi_models = ["LinearSVC", "SVC", "Ridge",
                "Logistic", "RandomForest", "XGB"]

algos = [LinearSVC(random_state=0), SVC(random_state=0), RidgeClassifier(random_state=0),
         LogisticRegression(random_state=0), RandomForestClassifier(
             random_state=0),
         XGBClassifier()]

parameters = {
    "LinearSVC": {"C": [0.1, 1, 10, 100], "penalty": ["l1", "l2"]},
    "SVC": {"kernel": ["linear", "rbf"], "C": [0.1, 1], "gamma": [0.01, 0.001]},
    "Ridge": {"alpha": [0.1, 1.0]},
    "Logistic": {"C": [0.1, 1], "penalty": ["l1", "l2"]},
    "RandomForest": {"n_estimators": [1000, 2000], "max_depth": [4, 10], "min_samples_split": [2, 5]},
    "XGB": {"learning_rate": [0.001, 0.01], "n_estimators": [1000, 2000], "max_depth": [4, 10], "subsample": [0.6, 0.8]}
}


# LinearSVC
# {'C': 0.1, 'penalty': 'l2'}
# SVC
# {'C': 1, 'gamma': 0.01, 'kernel': 'rbf'}
# Ridge
# {'alpha': 0.1}
# Logistic
# {'C': 0.1, 'penalty': 'l2'}
# RandomForest
# {'max_depth': 10, 'min_samples_split': 2, 'n_estimators': 2000}
# XGB
# {'learning_rate': 0.001, 'max_depth': 4, 'n_estimators': 2000, 'subsample': 0.6}


def solution(model):
    model.fit(X_train, y_train)
    return model


def find_score(model):
    pred = solution(model).predict(X_test)
    acc_score = accuracy_score(y_test, pred)
    return acc_score*100


# for i, j in zip(multi_models, algos):
#     print(i)
#     grid = GridSearchCV(
#         solution(j), param_grid=parameters[i], cv=10, n_jobs=-1)
#     grid.fit(X_train, y_train)
#     print(grid.best_params_)

algos_gridded = [
    LinearSVC(random_state=0, C=0.1, penalty="l2"),
    SVC(random_state=0, C=1, gamma=0.01),
    RidgeClassifier(random_state=0, alpha=0.1),
    LogisticRegression(random_state=0, C=0.1, penalty="l2"),
    RandomForestClassifier(random_state=0, max_depth=10,
                           min_samples_split=2, n_estimators=2000),
    XGBClassifier(learning_rate=0.001, max_depth=4,
                  n_estimators=2000, subsample=0.6)
]

success = []
for i in algos_gridded:
    success.append(find_score(i))

mlist = list(zip(multi_models, success))
result = pd.DataFrame(mlist, columns=["Model_Name", "Success"])
print(result.sort_values("Success", ascending=False))

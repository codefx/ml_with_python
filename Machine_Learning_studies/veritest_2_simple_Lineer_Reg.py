#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 16:07:02 2023

@author: cfxm
"""

import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
from sklearn.linear_model import LinearRegression as linreg

data = pd.read_csv("Salary.xls")
# data = pd.read_csv("Salary_Data.csv")

sal_data = data.copy()
# print(sal_data)
# y= sal_data["Salary"]
y = sal_data.Salary
# X = sal_data["YearsExperience"]
X = sal_data.YearsExperience

# plt.scatter(X, y)
# plt.show()

constant = sm.add_constant(X)
model = sm.OLS(y, constant).fit()
print(model.summary())

lr = linreg()
lr.fit(X.values.reshape(-1, 1), y.values.reshape(-1, 1))  # sklearn reshape
print(lr.coef_, lr.intercept_)
print(lr.predict(X.values.reshape(-1, 1)))

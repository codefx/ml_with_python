#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 12:24:56 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts, GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier, plot_tree


data = pd.read_csv("cancerdata.csv")  # same as logreg_data
cancer_data = data.copy()
# print(cancer_data.isnull().sum())
# print(cancer_data.head())
# print(cancer_data.info())
cancer_data = cancer_data.drop(columns=["id", "Unnamed: 32"], axis=1)
# print(cancer_data.info())
cancer_data.diagnosis = [1 if value ==
                         "M" else 0 for value in cancer_data.diagnosis]


y = cancer_data.diagnosis
X = cancer_data.drop(columns="diagnosis", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stsc = StandardScaler()
X_train = stsc.fit_transform(X_train)
X_test = stsc.transform(X_test)

dtc_model = DecisionTreeClassifier()
dtc_model.fit(X_train, y_train)
pred = dtc_model.predict(X_test)

accu_score = accuracy_score(y_test, pred)
print(accu_score)  # 0.9385964912280702

# xnames = list(X.columns)
# plot_tree(dtc_model, filled=True, fontsize=8, feature_names=xnames)
# plt.show()

# parameters = {"criterion": ["gini", "entropy", "log_loss"],
#               "max_leaf_nodes": range(2, 10),
#               "max_depth": range(2, 10),
#               "min_samples_split": range(2, 10),
#               "min_samples_leaf": range(2, 10)}

# grid = GridSearchCV(dtc_model, param_grid=parameters, cv=10, n_jobs=-1)
# grid.fit(X_train, y_train)
# print(grid.best_params_)
# {'criterion': 'gini', 'max_depth': 6, 'max_leaf_nodes': 9,
#               'min_samples_leaf': 3, 'min_samples_split': 2}


dtc_model2 = DecisionTreeClassifier(random_state=0, criterion="gini",
                                    max_depth=6, max_leaf_nodes=9,
                                    min_samples_leaf=3, min_samples_split=2)
dtc_model2.fit(X_train, y_train)
pred2 = dtc_model2.predict(X_test)

accu_score2 = accuracy_score(y_test, pred2)
print(accu_score2)  # 0.956140350877193


xnames = list(X.columns)
plot_tree(dtc_model2, filled=True, fontsize=8, feature_names=xnames)
plt.show()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 14:47:00 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split as tts
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR
import sklearn.metrics as mtr
import yfinance as yf


# data = yf.download("THYAO.IS", start="2023-08-01", end="2023-09-01")
# sp_data = data.copy()
# sp_data=sp_data.reset_index() #if first column not arranged
data2 = pd.read_csv("THYAO.IS.csv")
thy_data = data2.copy()
thy_data["Day"] = thy_data["Date"].astype(str).str.split("-").str[2]
# print(thy_data)

y = thy_data["Adj Close"]
X = thy_data["Day"]

y = np.array(y).reshape(-1, 1)
X = np.array(X).reshape(-1, 1)


ssx = StandardScaler()
ssy = StandardScaler()

y = ssy.fit_transform(y)
X = ssx.fit_transform(X)

# svr_model = SVR()  #default rbf
svr_model_rbf = SVR()
# svr_model_rbf = SVR(kernel="rbf", C=10000)
svr_model_lin = SVR(kernel="linear")
svr_model_poly = SVR(kernel="poly")

svr_model_rbf.fit(X, y)
pred_rbf = svr_model_rbf.predict(X)

# svr_model_lin.fit(X, y)
# pred_lin = svr_model_lin.predict(X)

# svr_model_poly.fit(X, y)
# pred_poly = svr_model_poly.predict(X)


r2 = mtr.r2_score(y, pred_rbf)
rmse = mtr.mean_squared_error(y, pred_rbf, squared=False)

print(f" R2 : {r2}  RMSE : {rmse}")

plt.scatter(X, y, color="Blue")
plt.plot(X, pred_rbf, color="green", label="RBF MODEL")
# plt.plot(X, pred_lin, color="red", label="LİNEAR MODEL")
# plt.plot(X, pred_poly, color="cyan", label="POLY MODEL")
plt.legend()
plt.show()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 24 21:42:11 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

from sklearn.model_selection import train_test_split as tts, cross_val_score
from sklearn.preprocessing import StandardScaler, OrdinalEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score


data = pd.read_csv("babies_food_Ingredients.csv")
i_data = data.copy()
# print(i_data)
i_data["size"].fillna(1, inplace=True)
bfi_data = i_data.drop(columns="Unnamed: 0", axis=1)
# print(bfi_data)

# print(bfi_data.quality.unique())
# this like how your data "Y column",
categories = ["-2", "-1", "0", "1", "2", "3"]
# str categories  etc to order it, ordinal encoder transform it to numerical order.
# normally in this data not needed,already was ordered that 0,1,2,3 we did for example.
ordenc = OrdinalEncoder(categories=[categories])
bfi_data["New_Qual"] = ordenc.fit_transform(
    bfi_data.quality.values.reshape(-1, 1))
# print(bfi_data[bfi_data["quality"] == 3][["quality", "New_Qual"]])
bfi_data = bfi_data.drop(columns="quality", axis=1)
# print(bfi_data)

y = bfi_data.New_Qual
X = bfi_data.drop(columns="New_Qual", axis=1)
X_train, X_test, y_train, y_test = tts(X, y, test_size=0.2, random_state=42)

stsc = StandardScaler()
X_train = stsc.fit_transform(X_train)
X_test = stsc.transform(X_test)

model_bfi_ord_loreg = LogisticRegression(random_state=0)
model_bfi_ord_loreg.fit(X_train, y_train)

pred = model_bfi_ord_loreg.predict(X_test)

confmat = confusion_matrix(y_test, pred)
print(confmat)
acc_score = accuracy_score(y_test, pred)
print(acc_score*100)

cvs = cross_val_score(model_bfi_ord_loreg, X_test, y_test, cv=10)
print(cvs)

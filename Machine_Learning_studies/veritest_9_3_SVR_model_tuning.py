#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 14:47:00 2023

@author: cfxm
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR
import sklearn.metrics as mtr
import yfinance as yf


data2 = pd.read_csv("THYAO.IS.csv")
thy_data = data2.copy()
thy_data["Day"] = thy_data["Date"].astype(str).str.split("-").str[2]


y = thy_data["Adj Close"]
X = thy_data["Day"]

y = np.array(y).reshape(-1, 1)
X = np.array(X).reshape(-1, 1)


ssx = StandardScaler()
ssy = StandardScaler()

y = ssy.fit_transform(y)
X = ssx.fit_transform(X)


# svr_model_rbf = SVR()
svr_model_rbf = SVR(kernel="rbf", C=100, gamma=1)

svr_model_rbf.fit(X, y)
pred_rbf = svr_model_rbf.predict(X)


r2 = mtr.r2_score(y, pred_rbf)
rmse = mtr.mean_squared_error(y, pred_rbf, squared=False)

print(f" R2 : {r2}  RMSE : {rmse}")

parameters = {"C": [1, 10, 100, 1000, 10000], "gamma": [
    1, 0.1, 0.001], "kernel": ["rbf", "linear", "poly"]}
parameters2 = {"C": [1, 10, 100, 1000, 10000], "gamma": [
    1, 0.1, 0.001], "kernel": ["linear", "poly"]}

# tuning = GridSearchCV(estimator=SVR(), param_grid=parameters, cv=10)
# tuning = GridSearchCV(estimator=SVR(), param_grid=parameters2, cv=10)
# tuning.fit(X, y.ravel())
# print(tuning.best_params_)
# 1 condition {'C': 100, 'gamma': 1, 'kernel': 'rbf'}
# 2 condition {'C': 10000, 'gamma': 1, 'kernel': 'linear'} if no rbf

plt.scatter(X, y, color="Blue")
plt.plot(X, pred_rbf, color="green", label="RBF MODEL")

plt.legend()
plt.show()
